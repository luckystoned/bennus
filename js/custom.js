/*GLOBAL VARS*/
var none = 'none';
var nav = $('.nav');
var arm = $('.arm');
var armRotate = 'arm-rotate';
var square = $('.square');
var squareRotate = 'square-rotate';
var shortArm = $('.short-arm');
var shortArmRotate = 'short-arm-rotate';
var shortArmE = 'short-arm-rotate-end';
var smallSquare = $('.small-square');
var smallSquareRotate = 'small-square-rotate';
var smallSquareE = 'small-square-rotate-end';
var ajax1 = $("#data1");
var ajax2 = $("#data2");

/*FUNCTIONS*/
function toggleNone(){
  nav.toggleClass(none);
}

function toggleRegular(){
  arm.toggleClass(armRotate);
  square.toggleClass(squareRotate);
}

function toggleSmall(){
  smallSquare.toggleClass(smallSquareE);
  shortArm.toggleClass(shortArmE);
}

function toggleRotateSmall(){
  shortArm.toggleClass(shortArmRotate);
  smallSquare.toggleClass(smallSquareRotate);
}

function checkNone() {
  if (!nav.hasClass(none)) {
    toggleNone()
  }
}

function checkArmRotate() {
  if (!arm.hasClass(armRotate)) {
    toggleRegular();
    toggleRotateSmall();
  }
}

function timeToggleNone() {
  setTimeout(function() {toggleNone();}, 1300);
}

function showStick() {
  $(".stick").show('blind', 2000);
  $(".stick-sm").show('blind', 2000);
  $(".stick-reg").show('blind', 2000);
}

function hideStick() {
  $(".stick").hide('fade', 500);
  $(".stick-sm").hide('fade', 500);
  $(".stick-reg").hide('fade', 500);
}

function toggleAjaxMsj1() {
  ajax1.show();
  ajax2.hide();
}
function toggleAjaxMsj2() {
  ajax2.show();
  ajax1.hide();
}
/*AJAX*/
$.ajax({
  url: 'https://jsonplaceholder.typicode.com/posts/1', 
  type: 'GET',
  dataType: 'json',
  success: function (data1) {
    var result1="";
    result1 += '<div>';
    result1 += '<h1 class="title-result">' + data1.title + '</h1>';
    result1 += '<p class="body-result">' + data1.body + '</p>';
    result1 += '</div>';
    ajax1.append(result1);
  },
  error: function (data1) {
  }
});

$.ajax({
  url: 'https://jsonplaceholder.typicode.com/posts/2', 
  type: 'GET',
  dataType: 'json',
  success: function (data2) {
    var result2="";
    result2 += '<div>';
    result2 += '<h1 class="title-result">' + data2.title + '</h1>';
    result2 += '<p class="body-result">' + data2.body + '</p>';
    result2 += '</div>';
    ajax2.append(result2);
  },
  error: function (data2) {
  }
});

/*ACTIONS*/
jQuery(document).ready(function($) {
  showStick();

  shortArm.on('click', function(event) {
    hideStick();
    toggleAjaxMsj1();
    if ($(this).hasClass(shortArmE)) {
      toggleSmall();
      checkArmRotate();
      checkNone();
    }else{      
      toggleRegular();
      toggleRotateSmall();
      checkNone();
    }
  });

  arm.on('click', function(event) {
    hideStick();
    toggleAjaxMsj2();
    if ($(this).hasClass(armRotate)) {
      toggleRegular();
      toggleRotateSmall();
      toggleSmall();
      timeToggleNone();
    }else{
      toggleNone();
    }
  });
});